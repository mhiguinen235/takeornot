//
//  TakeOrNotApp.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 15/10/2023.
//

import SwiftUI

@main
struct TakeOrNotApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
