//
//  ProfileFirebase.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation
import Firebase
import FirebaseFirestore

class ProfileManager {
    static let shared = ProfileManager()
    private let db = Firestore.firestore()

    // MARK: - Fetch User Profile

    func fetchUserProfile(userId: String, completion: @escaping (Result<Profile, Error>) -> Void) {
        db.collection("profiles").document(userId).getDocument { (document, error) in
            if let document = document, document.exists {
                let profile = try? document.data(as: Profile.self)
                completion(.success(profile))
            } else {
                completion(.failure(error ?? NSError(domain: "ProfileManager", code: -1, userInfo: nil)))
            }
        }
    }

    // MARK: - Update User Profile

    func updateUserProfile(_ profile: Profile, completion: @escaping (Result<Void, Error>) -> Void) {
        let profileDict = toDictionary(profile: profile)
        db.collection("profiles").document(profile.id).setData(profileDict) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }


    // MARK: - Delete User Profile

    func deleteUserProfile(userId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        db.collection("profiles").document(userId).delete() { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }
    
    // MARK: - Helper Methods

    private func toDictionary(profile: Profile) -> [String: Any] {
        var dict: [String: Any] = [
            "id": profile.id,
            "bio": profile.bio,
            "skills": profile.skills,
            "followers": profile.followers,
            "followings": profile.followings,
            "photoCouverture": profile.photoCouverture ?? "",
            "badges": profile.badges,
            "postes":profile.postes
        ]


        return dict
    }

}

