//
//  PostInteractionManager .swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation
import Firebase
import FirebaseFirestore

class PostInteractionManager {
    static let shared = PostInteractionManager()
    var commentManager = CommentaireManager()
    private let db = Firestore.firestore()

    // MARK: - Like and Unlike Post

    func toggleLike(onPostWithId postId: String, userId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let postRef = db.collection("posts").document(postId)
        
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            let postDocument: DocumentSnapshot
            do {
                try postDocument = transaction.getDocument(postRef)
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }

            guard var likes = postDocument.data()?["likes"] as? [String] else {
                errorPointer?.pointee = NSError(domain: "PostInteractionManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Impossible de récupérer les likes actuels"])
                return nil
            }

            if let index = likes.firstIndex(of: userId) {
                likes.remove(at: index)
            } else {
                likes.append(userId)
            }

            transaction.updateData(["likes": likes], forDocument: postRef)
            return nil
        }) { (_, error) in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }

    // MARK: - Share Post

    func sharePost(withId id: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let postRef = db.collection("posts").document(id)
        postRef.updateData(["nombrePartage": FieldValue.increment(Int64(1))]) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }

    // MARK: - Comment on Post

    func CommentPost(withId id: String,comment:String, completion: @escaping (Result<Void, Error>) -> Void) {
        let commentaire =  Commentaire(likes: 0, idUtilisateur:"" , nombreReponses: 0, dateHeure: Date(), signaler: false)
        commentManager.addComment(toPostWithId: id, commentaire: commentaire) { error  in
            
        }
    }
    // Cette fonction pourrait être ajoutée si vous souhaitez gérer les commentaires ici

    // ... autres méthodes d'interaction avec les posts ...
    


}
