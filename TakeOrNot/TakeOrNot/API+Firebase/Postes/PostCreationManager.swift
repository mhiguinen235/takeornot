//
//  PostCreationManager.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//


import Foundation
import Firebase
import FirebaseFirestore

class PostCreationManager {
    static let shared = PostCreationManager()
    private let db = Firestore.firestore()

    // MARK: - Create Post

    func createPost(_ post: Poste, completion: @escaping (Result<Void, Error>) -> Void) {
        db.collection("posts").addDocument(data: toDictionary(post: post)) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }

    // MARK: - Helper Method

    private func toDictionary(post: Poste) -> [String: Any] {
        return [
            "id": post.id,
            "datePublication": post.datePublication,
            "defiInitId": post.defiInitId,
            "commentaires": post.commentaires,
            "owner": post.owner,
            "vue": post.vue,
            "nombrePartage": post.nombrePartage,
            "nombreLike": post.nombreLike,
            "legende": post.legende,
            "hashtags": post.hashtags,
            "statutDefi": post.statutDefi.rawValue,
            "categorie": post.categorie ?? "",
            "tags": post.tags,
            "urlVideo": post.urlVideo
        ]
    }
}
