//
//  AuthEmailFirebase.swift
//  revizite
//
//  Created by Mathis Higuinen on 10/12/2023.
//

import Foundation
import Firebase
import Combine
import FirebaseAuth

class AuthEmailFirebase: ObservableObject {
    
    @Published var currentUser: User?
    static let shared = AuthEmailFirebase()
    private var authListenerHandle: AuthStateDidChangeListenerHandle?

    init() {
        authListenerHandle = setupAuthStateListener()
    }

    private func setupAuthStateListener() -> AuthStateDidChangeListenerHandle {
        return Auth.auth().addStateDidChangeListener { [weak self] (auth, user) in
            self?.currentUser = user
        }
    }

    // MARK: - Authentication Methods

    func login(withEmail email: String, password: String, completion: @escaping (Result<User, AuthError>) -> Void) {
        let signInOperation: AuthOperation = { email, password, completion in
            Auth.auth().signIn(withEmail: email, password: password, completion: completion)
        }

        performAuthOperation(email: email, password: password, authOperation: signInOperation) { result in
            completion(result)
        }
    }


    func signup(withEmail email: String, password: String, completion: @escaping (Result<User, AuthError>) -> Void) {
        validateEmailFormat(email) { [weak self] isValid in
            guard isValid else {
                completion(.failure(.invalidEmailDomain))
                return
            }
            self?.performAuthOperation(email: email, password: password, authOperation: Auth.auth().createUser) { result in
                completion(result)
            }
        }
    }

    func signout(completion: @escaping (Error?) -> Void) {
        do {
            try Auth.auth().signOut()
            completion(nil)
        } catch {
            completion(error)
        }
    }

    // MARK: - Helper Methods

    typealias AuthOperation = (_ email: String, _ password: String, _ completion: @escaping (AuthDataResult?, Error?) -> Void) -> Void
    
    private func performAuthOperation(email: String, password: String, authOperation: AuthOperation, completion: @escaping (Result<User, AuthError>) -> Void) {
        authOperation(email, password) { [weak self] (result, error) in
            if let error = error {
                self?.handleAuthError(error, completion: completion)
            } else if let user = result?.user {
                completion(.success(user))
            } else {
                completion(.failure(.unknown))
            }
        }
    }
    
    
    func updatePassword(currentPassword: String, newPassword: String, completion: @escaping (Result<Void, AuthError>) -> Void) {
        guard let user = Auth.auth().currentUser, let email = user.email else {
            completion(.failure(.userNotFound))
            return
        }

        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
        user.reauthenticate(with: credential) { [weak self] _, error in
            if let error = error {
                self?.handleAuthError(error) { result in
                    switch result {
                    case .success:
                        // Mise à jour du mot de passe
                        user.updatePassword(to: newPassword) { error in
                            if let error = error {
                                print("Error update password")
                            } else {
                                completion(.success(()))
                            }
                        }
                    case .failure(let authError):
                        completion(.failure(authError))
                    }
                }
                return
            }
        }
    }
    
    func sendPasswordReset(email: String, completion: @escaping (Result<Void, AuthError>) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { error in
            if let error = error {
                // Gérer l'erreur
                self.handleAuthError(error) { result in
                    switch result {
                    case .failure(let authError):
                        completion(.failure(authError))
                    case .success:
                        completion(.success(()))
                    }
                }
            } else {
                // Email de réinitialisation envoyé avec succès
                completion(.success(()))
            }
        }
    }

   
    private func handleAuthError(_ error: Error, completion: (Result<User, AuthError>) -> Void) {
        if let errorCode = AuthErrorCode.Code(rawValue: (error as NSError).code) {
            switch errorCode {
            case .invalidEmail:
                completion(.failure(.invalidEmail))
            case .wrongPassword:
                completion(.failure(.wrongPassword))
            // ... ajoutez d'autres cas selon les besoins
            case . userNotFound:
                completion(.failure(.userNotFound))
            default:
                completion(.failure(.unknown))
            }
        } else {
            completion(.failure(.unknown))
        }
    }
    

    private func validateEmailFormat(_ email: String, completion: @escaping (Bool) -> Void) {
        let isValidFormat = email.contains("@")
        completion(isValidFormat)
    }


}
