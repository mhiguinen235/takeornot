//
//  CommentaireManager.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation
import Firebase
import FirebaseFirestore

class CommentaireManager {
    static let shared = CommentaireManager()
    private let db = Firestore.firestore()

    // MARK: - Ajouter un Commentaire

    func addComment(toPostWithId postId: String, commentaire: Commentaire, completion: @escaping (Result<Void, Error>) -> Void) {
        let postRef = db.collection("posts").document(postId)
        db.collection("commentaires").addDocument(data: toDictionary(commentaire: commentaire)) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                // Optionnellement, mettre à jour le post avec le nouveau commentaire
                postRef.updateData(["commentaires": FieldValue.arrayUnion([commentaire.id])]) { error in
                    if let error = error {
                        completion(.failure(error))
                    } else {
                        completion(.success(()))
                    }
                }
            }
        }
    }

    // MARK: - Supprimer un Commentaire

    func deleteComment(withId commentId: String, fromPostId postId: String, userId: String, completion: @escaping (Result<Void, Error>) -> Void) {
            let postRef = db.collection("posts").document(postId)
            let commentRef = db.collection("commentaires").document(commentId)

            commentRef.getDocument { (document, error) in
                if let document = document, document.exists {
                    let commentOwnerId = document.get("ownerId") as? String
                    let postOwnerId = document.get("postOwnerId") as? String

                    if userId == commentOwnerId || userId == postOwnerId {
                        commentRef.delete() { error in
                            if let error = error {
                                completion(.failure(error))
                            } else {
                                postRef.updateData(["commentaires": FieldValue.arrayRemove([commentId])]) { error in
                                    if let error = error {
                                        completion(.failure(error))
                                    } else {
                                        completion(.success(()))
                                    }
                                }
                            }
                        }
                    } else {
                        completion(.failure(NSError(domain: "CommentaireManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Vous n'avez pas la permission de supprimer ce commentaire."])))
                    }
                } else if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.failure(NSError(domain: "CommentaireManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Commentaire introuvable."])))
                }
            }
        }

    // MARK: - Récupérer les Commentaires d'un Post

    func fetchComments(forPostId postId: String, completion: @escaping (Result<[Commentaire], Error>) -> Void) {
        db.collection("commentaires").whereField("postId", isEqualTo: postId).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            } else if let snapshot = snapshot {
                let commentaires = snapshot.documents.compactMap { document -> Commentaire? in
                    return try? document.data(as: Commentaire.self)
                }
                completion(.success(commentaires))
            }
        }
    }
    
    
    func replyToComment(commentId: String, withReply reply: Commentaire, completion: @escaping (Result<Void, Error>) -> Void) {
        // Définir l'ID du commentaire parent dans la réponse
        var replyWithParent = reply
        replyWithParent.idCommentaireParent = commentId

        // Ajouter le commentaire de réponse dans la collection des commentaires
        db.collection("commentaires").addDocument(data: toDictionary(commentaire: reply)) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                // Optionnellement, mettre à jour le post avec le nouveau commentaire de réponse
                // Si vous avez une référence au post dans votre structure `Commentaire`
                if let postId = replyWithParent.postId {
                    let postRef = db.collection("posts").document(postId)
                    postRef.updateData(["commentaires": FieldValue.arrayUnion([replyWithParent.id])]) { error in
                        if let error = error {
                            completion(.failure(error))
                        } else {
                            completion(.success(()))
                        }
                    }
                } else {
                    completion(.success(()))
                }
            }
        }
    }


    // MARK: - Helper Method

    private func toDictionary(commentaire: Commentaire) -> [String: Any] {
        // Convert Commentaire to dictionary for Firestore
        // Implement this based on your Commentaire structure
    }
}
