//
//  PostManagerFirebase.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//


import Foundation
import Firebase
import FirebaseFirestore

class PostManager {
    static let shared = PostManager()
    private let db = Firestore.firestore()

    // MARK: - Create Post

    func createPost(_ post: Poste, completion: @escaping (Result<Void, Error>) -> Void) {
        var ref: DocumentReference? = nil
        ref = db.collection("posts").addDocument(data: toDictionary(post: post)) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }

    // MARK: - Read Post(s)

    func fetchPost(byId id: String, completion: @escaping (Result<Poste, Error>) -> Void) {
        db.collection("posts").document(id).getDocument { (document, error) in
            if let document = document, document.exists {
                let post = try? document.data(as: Poste.self)
                completion(.success(post))
            } else {
                completion(.failure(error ?? NSError(domain: "PostManager", code: -1, userInfo: nil)))
            }
        }
    }


    // MARK: - Update Post

    func updatePost(_ post: Poste, completion: @escaping (Result<Void, Error>) -> Void) {
        db.collection("posts").document(post.id).setData(toDictionary(post: post)) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }

    // MARK: - Delete Post

    func deletePost(withId id: String, completion: @escaping (Result<Void, Error>) -> Void) {
        db.collection("posts").document(id).delete() { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }
    
    
    func fetchPosts(limit: Int, startAfter: DocumentSnapshot? = nil, completion: @escaping (Result<[Poste], Error>) -> Void) {
        var query: Query = db.collection("posts").order(by: "datePublication").limit(to: limit)
        if let startAfterDoc = startAfter {
            query = query.start(afterDocument: startAfterDoc)
        }
        query.getDocuments { (snapshot, error) in
            // Gérer la récupération des documents
        }
    }



    func sharePost(withId id: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let postRef = db.collection("posts").document(id)
        postRef.updateData(["nombrePartage": FieldValue.increment(Int64(1))]) { error in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }


    // MARK: - Fetch Challenge Details

    func fetchChallengeDetails(forPost postId: String, completion: @escaping (Result<Challenge, Error>) -> Void) {
        db.collection("posts").document(postId).getDocument { (document, error) in
            if let document = document, let defiInitId = document.get("defiInitId") as? String {
                self.db.collection("challenges").document(defiInitId).getDocument { (challengeDocument, error) in
                    if let challengeDocument = challengeDocument, challengeDocument.exists {
                        let challenge = try? challengeDocument.data(as: Challenge.self)
                        completion(.success(challenge))
                    } else {
                        completion(.failure(error ?? NSError(domain: "PostManager", code: -1, userInfo: nil)))
                    }
                }
            } else {
                completion(.failure(error ?? NSError(domain: "PostManager", code: -1, userInfo: nil)))
            }
        }
    }
    
    

    // MARK: - Search Posts by Hashtag or Tag

    func searchPosts(byHashtag hashtag: String, completion: @escaping (Result<[Poste], Error>) -> Void) {
        db.collection("posts").whereField("hashtags", arrayContains: hashtag).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            } else if let snapshot = snapshot {
                let posts = snapshot.documents.compactMap { document -> Poste? in
                    return try? document.data(as: Poste.self)
                }
                completion(.success(posts))
            }
        }
    }


    func searchPosts(byTag tag: String, completion: @escaping (Result<[Poste], Error>) -> Void) {
        db.collection("posts").whereField("tags", arrayContains: tag).getDocuments { (snapshot, error) in
            if let error = error {
                completion(.failure(error))
            } else if let snapshot = snapshot {
                let posts = snapshot.documents.compactMap { document -> Poste? in
                    return try? document.data(as: Poste.self)
                }
                completion(.success(posts))
            }
        }
    }
    
    
    func toggleLike(onPostWithId postId: String, userId: String, completion: @escaping (Result<Void, Error>) -> Void) {
        let postRef = db.collection("posts").document(postId)
        
        db.runTransaction({ (transaction, errorPointer) -> Any? in
            let postDocument: DocumentSnapshot
            do {
                try postDocument = transaction.getDocument(postRef)
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }

            guard var likes = postDocument.data()?["likes"] as? [String] else {
                errorPointer?.pointee = NSError(domain: "PostManager", code: -1, userInfo: [NSLocalizedDescriptionKey: "Impossible de récupérer les likes actuels"])
                return nil
            }

            if let index = likes.firstIndex(of: userId) {
                likes.remove(at: index)
            } else {
                likes.append(userId)
            }

            transaction.updateData(["likes": likes], forDocument: postRef)
            return nil
        }) { (_, error) in
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(()))
            }
        }
    }



    // MARK: - Helper Method

    private func toDictionary(post: Poste) -> [String: Any] {
        return [
            "id": post.id,
            "datePublication": post.datePublication,
            "defiInitId": post.defiInitId,
            // Convertir 'commentaires' si nécessaire, ou les stocker séparément
            "commentaires" : post.commentaires,
            "owner": post.owner,
            "vue": post.vue,
            "nombrePartage": post.nombrePartage,
            "nombreLike": post.nombreLike,
            "legende": post.legende,
            "hashtags": post.hashtags,
            "statutDefi": post.statutDefi.rawValue,
            "categorie": post.categorie ?? "",
            "tags": post.tags,
            "urlVideo": post.urlVideo
        ]
    }
    
    

}
