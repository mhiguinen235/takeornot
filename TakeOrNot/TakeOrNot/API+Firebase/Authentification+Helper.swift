//
//  Authentification+Helper.swift
//  revizite
//
//  Created by Mathis Higuinen on 10/12/2023.
//

import Foundation

enum AuthError: Error {
    case emailAlreadyInUse
    case invalidEmail
    case wrongPassword
    case unknown
    case invalidEmailDomain
    case userNotFound
}


enum SignUpError:Error{
    case faild
 
    
}

enum loginError:Error{
    case loginError
}
