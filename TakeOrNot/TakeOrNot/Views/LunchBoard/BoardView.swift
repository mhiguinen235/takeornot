//
//  BoardView.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 13/12/2023.
//

import SwiftUI
import Foundation

struct board:Identifiable,Hashable{
    var id = UUID().uuidString
    var image:String
    var name:String
    var desc:String
}

struct BoardView: View {
    
    var boards:[board] = [board(image: "image1", name: "Challenge", desc:"""
                                   You can challenge the videos of other users and can win the challenge andrewards as well
                                   """),board(image: "image2", name: "Share", desc:
                                   """
                                   You can share your video with other users
                                   and friends and can earn rewards by
                                   sharing your link
                                   """
),board(image: "image3", name: "Skills", desc: """
You can earn skills by participating in
challenges, as well as you can  buy
skill to enhance your activity
""")]
    
    @State var selectedElement:Int = 0
    
    func switchElement(){
        if selectedElement < 2 {
            selectedElement += 1
        }
        else{
            selectedElement = 0
        }
    }
    
    var body: some View {
        VStack{
            Spacer()
            ZStack {
                          ForEach(boards) { board in
                              if boards.firstIndex(of: board) == selectedElement {
                                  lunchFormView(image: board.image, title: board.name, desc: board.desc)
                              }
                          }.onTapGesture {
                              selectedElement = (selectedElement + 1) % boards.count
                          }
                      }
            Spacer()
            HStack{
                Image(systemName: selectedElement == 0 ? "circle.fill" : "circle")
                    
                Image(systemName: selectedElement == 1 ? "circle.fill" : "circle")
                Image(systemName: selectedElement == 2 ? "circle.fill" : "circle")
            }.foregroundColor(Color("gradient"))
            Spacer()
          
            GetStartedButton()
          
        }.padding()
    }
}

#Preview {
    BoardView()
}



struct GetStartedButton: View {
    var body: some View {
        Button {
            
        } label: {
            HStack{
                Spacer()
                Text("Get Started")
                    .foregroundStyle(.white)
                    .bold()
                    .padding()
                Spacer()
            }
            .padding(.horizontal)
        }
        .background(
            LinearGradient(gradient: Gradient(colors: [Color("primary"), Color("gradient")]), startPoint: .top, endPoint: .bottom)
        )
        .cornerRadius(100)
    }
}

