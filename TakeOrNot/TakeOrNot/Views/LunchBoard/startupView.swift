//
//  startupView.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 13/12/2023.
//

import Foundation
import SwiftUI


struct startUpView: View {
    var body: some View {
        VStack{
            Spacer()
            Image("logo")
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(maxWidth:200,maxHeight: 200)
            Spacer()
        }
    }
}


#Preview {
    startUpView()
}
