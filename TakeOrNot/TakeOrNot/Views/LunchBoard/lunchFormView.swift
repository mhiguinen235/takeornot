//
//  lunchFormView.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 13/12/2023.
//

import SwiftUI

struct lunchFormView: View {
    var image:String = "image1"
    var title:String = "Challenge"
    var desc:String = "You can challenge the videos of other user and can win the challenge and rewards as well"
    
    var body: some View {
        VStack{
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fill)
               .frame(maxWidth:300,maxHeight: 300)
            VStack(spacing:16){
                
                Text(title)
                    .font(.largeTitle.bold())
                
                
                Text(desc)
                    .bold()
                    .multilineTextAlignment(.center)
            }
            
            
        }.foregroundColor(Color("gray"))
            .padding()
            .background(.white)
    }
}

#Preview {
    lunchFormView()
}
