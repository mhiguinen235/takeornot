//
//  Signalement.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//


import Foundation

struct Signalement: Codable {
    var id: String
    var idContenuSignale: String  // ID du post, commentaire, etc.
    var typeContenu: TypeContenu  // Enum pour spécifier le type (post, commentaire, etc.)
    var idAuteurContenu: String   // ID de l'auteur du contenu signalé
    var idSignaleur: String       // ID de l'utilisateur qui signale
    var raison: RaisonSignalement // Enum des raisons possibles du signalement
    var dateSignalement: Date
    var statut: StatutSignalement // Enum pour suivre le statut du signalement
    
    enum CodingKeys: CodingKey {
        case id
        case idContenuSignale
        case typeContenu
        case idAuteurContenu
        case idSignaleur
        case raison
        case dateSignalement
        case statut
    }
}

enum TypeContenu: String, Codable {
    case poste, commentaire, profile
}

enum RaisonSignalement: String, Codable {
    case spam, harcelement, contenuInapproprie, autre
}

enum StatutSignalement: String, Codable {
    case enAttente, enCoursDeTraitement, resolu, rejeté
}
