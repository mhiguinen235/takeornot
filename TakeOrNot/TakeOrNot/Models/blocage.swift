//
//  blocage.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation

struct Blocage: Codable {
    var id: String
    var idBloqueur: String
    var idBloque: String
    var dateBlocage: Date
    
    enum CodingKeys: CodingKey {
        case id
        case idBloqueur
        case idBloque
        case dateBlocage
    }
}
