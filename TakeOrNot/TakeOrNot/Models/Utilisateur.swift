//
//  Utilisateur.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 15/10/2023.
//


import Foundation

struct Profile: Codable {
    var id: String
    var bio: String
    var postes: [String]  // Assurez-vous que `Poste` est également `Codable`
    var skills: Int
    var followers: [String]
    var followings: [String]
    var photoCouverture: String?
    var badges: [String]
    
    enum CodingKeys: CodingKey {
        case id
        case bio
        case postes
        case skills
        case followers
        case followings
        case photoCouverture
        case badges
    }
}
