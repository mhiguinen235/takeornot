//
//  Challenge.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 15/10/2023.
//

import Foundation


struct Challenge: Codable {
    var titre: String
    var coverImage: String
    var skille: Int
    var id: String
    var dateValidite: Date
    var nombreConcurrentMax: Int
    var nombreInscrits: Int
    var warning:String
    var description:String
    
    enum CodingKeys: CodingKey {
        case titre
        case coverImage
        case skille
        case id
        case dateValidite
        case nombreConcurrentMax
        case nombreInscrits
        case warning
        case description
    }
}
