//
//  Ami.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation

struct Ami: Codable {
    var id: String
    var idUtilisateur1: String
    var idUtilisateur2: String
    var dateAmi: Date
    
    enum CodingKeys: CodingKey {
        case id
        case idUtilisateur1
        case idUtilisateur2
        case dateAmi
    }
}
