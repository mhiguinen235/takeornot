//
//  Message.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 15/10/2023.
//

import Foundation

enum StatutLecture: String, Codable {
    case lu, nonLu
}

struct Message: Codable {
    var envoyeA: String
    var recuDe: String
    var dateEnvoi: Date
    var image: String?
    var video: String?
    var photo: String?
    var poste: Poste?  // Assurez-vous que `Poste` est également `Codable`
    var message: String
    var id: String
    var statutLecture: StatutLecture
    var parent_id:String?
    
    enum CodingKeys: CodingKey {
        case envoyeA
        case recuDe
        case dateEnvoi
        case image
        case video
        case photo
        case poste
        case message
        case id
        case statutLecture
        case parent_id
    }
}
