//
//  conversation.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation

struct Conversation: Codable, Identifiable {
    var id: String
    var participants: [String] // ID des utilisateurs participants
    var derniereMiseAJour: Date // Date de la dernière activité dans la conversation
    var dernierMessage: String? // Aperçu ou contenu du dernier message envoyé
    var messages: [Message] // Optionnel, peut être utilisé si vous chargez tous les messages en mémoire
    
    enum CodingKeys: CodingKey {
        case id
        case participants
        case derniereMiseAJour
        case dernierMessage
        case messages
    }
}
