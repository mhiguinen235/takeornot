//
//  Notification.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//


import Foundation

struct Notification: Codable, Identifiable {
    var id: String
    var type: TypeNotification
    var idUtilisateurSource: String // ID de l'utilisateur qui a déclenché la notification
    var idUtilisateurDestinataire: String // ID de l'utilisateur destinataire de la notification
    var idContenuAssocie: String? // ID du contenu associé (poste, commentaire, etc.)
    var dateNotification: Date
    var lue: Bool // Indique si la notification a été lue
    
    enum CodingKeys: CodingKey {
        case id
        case type
        case idUtilisateurSource
        case idUtilisateurDestinataire
        case idContenuAssocie
        case dateNotification
        case lue
    }
}

enum TypeNotification: String, Codable {
    case nouveauCommentaire
    case nouveauFollower
    case likePoste
    case mention
    // Autres types de notifications
}
