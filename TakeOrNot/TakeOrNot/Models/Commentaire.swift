//
//  Commentaire.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 19/01/2024.
//

import Foundation

struct Commentaire: Codable {
    var id: String?
    var idCommentaireParent: String?  // ID du commentaire parent pour les réponses
    var likes: Int
    var idUtilisateur: String  // ID de l'utilisateur qui a écrit le commentaire
    var reaction: String?
    var nombreReponses: Int  // Nombre de réponses à ce commentaire
    var dateHeure: Date
    var signaler: Bool
    
    enum CodingKeys: CodingKey {
        case id
        case idCommentaireParent
        case likes
        case idUtilisateur
        case reaction
        case nombreReponses
        case dateHeure
        case signaler
    }
}
