//
//  poste.swift
//  TakeOrNot
//
//  Created by Mathis Higuinen on 15/10/2023.
//

import Foundation
import Firebase
import FirebaseStorage
import FirebaseFirestore
import FirebaseDatabaseSwift
import FirebaseFirestoreSwift

enum StatutDefi: String, Codable {
    case enCours, termine, limite
}

struct Poste: Codable, Identifiable{
    var id: String
    var datePublication: Date
    var defiInitId: String
    var commentaires: [String] // Assurez-vous que `Commentaire` est également `Codable`
    var owner: String
    var vue: Int
    var nombrePartage: Int
    var nombreLike: [String]
    var legende: String
    var hashtags: [String]
    var statutDefi: StatutDefi
    var categorie: String?
    var tags: [String]
    var urlVideo: String
    
    enum CodingKeys: CodingKey {
        case id
        case datePublication
        case defiInitId
        case commentaires
        case owner
        case vue
        case nombrePartage
        case nombreLike
        case legende
        case hashtags
        case statutDefi
        case categorie
        case tags
        case urlVideo
    }
}
