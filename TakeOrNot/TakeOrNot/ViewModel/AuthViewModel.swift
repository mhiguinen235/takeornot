//
//  AuthViewModel.swift
//  revizite
//
//  Created by Mathis Higuinen on 10/12/2023.
//

import Foundation
import Combine
import Firebase

class AuthViewModel: ObservableObject {
    @Published var currentUser: User?
    @Published var isAuthenticated: Bool = false
    @Published var errorMessage: String?

    private var authManager = AuthEmailFirebase.shared
    private var cancellables = Set<AnyCancellable>()

    init() {
        // S'abonner aux mises à jour de l'utilisateur
        authManager.$currentUser
            .receive(on: RunLoop.main)
            .sink { [weak self] user in
                self?.currentUser = user
                self?.isAuthenticated = user != nil
            }
            .store(in: &cancellables)
    }

    func login(email: String, password: String) {
        authManager.login(withEmail: email, password: password) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let user):
                    self?.currentUser = user
                    self?.errorMessage = nil
                    self?.isAuthenticated = true
                case .failure(let error):
                    self?.errorMessage = self?.parseAuthError(error)
                    self?.isAuthenticated = false
                }
            }
        }
    }

    func signup(email: String, password: String) {
        authManager.signup(withEmail: email, password: password) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let user):
                    self?.currentUser = user
                    self?.errorMessage = nil
                    self?.isAuthenticated = true
                case .failure(let error):
                    self?.errorMessage = self?.parseAuthError(error)
                    self?.isAuthenticated = false
                }
            }
        }
    }

    func signout() {
        authManager.signout { [weak self] error in
            DispatchQueue.main.async {
                if let error = error {
                    self?.errorMessage = error.localizedDescription
                } else {
                    self?.currentUser = nil
                    self?.isAuthenticated = false
                }
            }
        }
    }

    func updatePassword(currentPassword: String, newPassword: String) {
        authManager.updatePassword(currentPassword: currentPassword, newPassword: newPassword) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    self?.errorMessage = nil
                case .failure(let error):
                    self?.errorMessage = self?.parseAuthError(error)
                }
            }
        }
    }
    
    
    func resetPassword(email: String) {
        authManager.sendPasswordReset(email: email) { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success:
                    // Gérer le succès, par exemple en affichant un message à l'utilisateur
                    self?.errorMessage = "Un email de réinitialisation a été envoyé."
                case .failure(let error):
                    // Gérer l'échec
                    self?.errorMessage = self?.parseAuthError(error)
                }
            }
        }
    }


    private func parseAuthError(_ error: AuthError) -> String {
        // Transformez l'erreur AuthError en un message d'erreur compréhensible
        switch error {
        case .invalidEmail:
            return "Adresse e-mail invalide."
        case .wrongPassword:
            return "Mot de passe incorrect."
        case .userNotFound:
            return "Oups.. impossible de vous trouver"
        // ... autres cas d'erreurs
        default:
            return "Une erreur inconnue est survenue."
        }
    }
}
